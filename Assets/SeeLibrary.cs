﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SeeLibrary : MonoBehaviour {
	float t=0;
	public GameObject AddLibrary, InfoPanel;
	bool ifer=false;
	void Start () {
		t = 0;
	}

	void OnDrag () {
		t += Time.deltaTime;
		Debug.Log (t);
	}
	
	void OnDragOut () {
		if (t > 1) {
			if (transform.GetChild (0).GetComponent<UIWidget> ().alpha < 1f) transform.GetChild (0).GetComponent<UIWidget> ().alpha = 1;
			else transform.GetChild (0).GetComponent<UIWidget> ().alpha = 0;
		}
		t = 0;
	}
	
	void OnClick () {
		if (transform.GetChild (0).GetComponent<UIWidget> ().alpha == 1f) {	
			InfoPanel.GetComponent<UILabel> ().text = "Медээлел\n\n";
			ifer = !ifer;
			if (ifer) {
				string path = Path.GetDirectoryName(AddLibrary.GetComponent<GoSplitBook> ().copiedFile);
				
				DirectoryInfo dir = new DirectoryInfo(path);
				FileInfo[] info = dir.GetFiles("*.*");
				foreach (FileInfo f in info) {
					Debug.Log (f.Name + ". Lenght:" + f.Length + ". CreationTime:" + f.CreationTime);
					InfoPanel.GetComponent<UILabel> ().text +=f.Name + ". Lenght:" + f.Length + ". CreationTime:" + f.CreationTime+"\n"+"\n";
				}
				InfoPanel.GetComponent<UIWidget> ().alpha = 1f;
			}
			else InfoPanel.GetComponent<UIWidget> ().alpha = 0;
		}
	}
}