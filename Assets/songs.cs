﻿using UnityEngine;
using System.Collections;
using System.IO;

public class songs : MonoBehaviour {
	public int n, m;
	public UILabel search, searchbackl;
	public GameObject[] lines;
	public GameObject BoxBlock;
	public GameObject texter;
	public Transform Parent;
	public GameObject TextPanel, BottomLabel;
	public UIScrollView SCV;
	StringReader file = null, filewords=null;
	string line;
	public TextAsset text, wtext;
	string mdata, wdata;
	bool blockOnChange = false;
	bool trueword = false;

	public void Start () {
		TouchScreenKeyboard.hideInput = true;
		trueword = true;
		mdata = text.text;
		wdata = wtext.text;
		BoxBlock.SetActive (false);
		texter.GetComponent<UIWidget> ().width = TextPanel.GetComponent<UIWidget> ().width-10;
		search.GetComponent<UIInput> ().selectAllTextOnFocus = true;
	}
	
	public void first () {
		TouchScreenKeyboard.hideInput = true;
		if (search.text != "") {
			if (!blockOnChange) {
				BoxBlock.SetActive (false);
				texter.SetActive (false);
				for (int h=0; h<m; h++)	lines [h].GetComponent<UILabel> ().text = "";
				string st = search.text;			
				int ii = wdata.IndexOf ("\n" + st);

				if (ii < 0) {
					BottomLabel.GetComponent<UILabel> ().text = "Ындыг сөс тывылбаан";
					trueword = false;
					return;
				} else
					BottomLabel.GetComponent<UILabel> ().text = "Одуругларны арыглаар";
				trueword = true;
				filewords = new StringReader (wdata.Substring (ii));
				int i = 0;
				int j = 0;
				int k = 0;		
				if (filewords != null) {
					while ((line = filewords.ReadLine())!=null) {
						if (st.Length != 0) {
							if (line.Length >= st.Length)
							if (st == line.Substring (0, st.Length)) {
								lines [j].GetComponent<UILabel> ().text = line.Trim ();
								++j;
								break;
							}
						} else
							break;
						++i;
					}
					while (((line = filewords.ReadLine())!=null) && j<m && k<20) {
						if ((j < m) && (line.Length >= st.Length)) {
							if (st == line.Substring (0, st.Length)) {
								lines [j].GetComponent<UILabel> ().text = line.Trim ();
								lines [j].GetComponent<BoxCollider> ().enabled = true;
								++j;
							}
						}
						++i;
						k++;
					}
				}
			} else
				blockOnChange = false;
		} else {
			search.text = "борта бижи..."; 
			BottomLabel.GetComponent<UILabel> ().text = "";
		}
	}

	public void afterSubmit () {
		search.text = searchbackl.text;
		blockOnChange = true;
		search.GetComponent<UIInput> ().value = searchbackl.text;
		searchbackl.enabled = false;
		search.enabled = true;
		search.GetComponent<UIInput> ().isSelected = true;

	}

	public void goClear () {
		search.text="";
		blockOnChange = true;
		search.GetComponent<UIInput> ().value = "";
		searchbackl.enabled = false;
		search.enabled = true;
		texter.GetComponent<UILabel> ().text = "";

		search.GetComponent<UIInput> ().RemoveFocus ();

		foreach (GameObject l in GameObject.FindGameObjectsWithTag("lines"))
		{
			l.GetComponent<UILabel> ().text = "";
		}

		search.text = "борта бижи..."; 
		BottomLabel.GetComponent<UILabel> ().text = "";
	}

	public void goSubmit () {
		if (search.text != "") {
			if (!search.enabled) afterSubmit ();
				else gosearch (search.text);
			} else search.text = "борта бижи...";
	}

	public void gosearch (string sline) {
		if (!trueword) return;
		BoxBlock.SetActive (true);
		for (int h=0; h<m; h++) lines[h].GetComponent<UILabel> ().text = "";

		int ii = mdata.IndexOf("\n"+sline);
		texter.SetActive (true);
		if (sline.Length != 0 && ii>0) {
			texter.GetComponent<UILabel> ().text = "";

			file = new StringReader (mdata.Substring(ii));
			line = file.ReadLine(); //пустая строка
			line = file.ReadLine(); //искомое слово
			searchbackl.text = line.Trim();

			Debug.Log(line);
			int j = 0;
			while (line !="\t" && j<100) { //последующие строки за словом
				if (line.Substring (0, 1)=="\t") texter.GetComponent<UILabel> ().text += line+"\n";
				line = file.ReadLine();
				j++;
			}
			Debug.Log(texter.GetComponent<UILabel> ().text);
			texter.GetComponent<UILabel> ().bottomAnchor.relative = 1-j*0.05f;
				}
		SCV.transform.localPosition = new Vector3 (0, TextPanel.GetComponent<UIWidget> ().height/2 -10 - texter.GetComponent<UILabel> ().height/2, 0);
		search.enabled = false;
		search.GetComponent<UIInput> ().RemoveFocus ();
		searchbackl.enabled = true;
		}

	string Parsing (string s) {
		s = s.Replace ("[m1]", "");
		s = s.Replace ("[m2]", " ");
		s = s.Replace ("[/m]", "");


		s = s.Replace ("[trn]", "");
		s = s.Replace ("[/trn]", "");
		s = s.Replace ("[!trs]", "");
		s = s.Replace ("[/!trs]", "");

		s = s.Replace ("[ex]", "[808080]");
		s = s.Replace ("[/ex]", "[-]");
		s = s.Replace ("[ref]", "[i][0099BB]");
		s = s.Replace ("[/ref]", "[-][/i]");

		s = s.Replace ("[lang id=1]", "");
		s = s.Replace ("[lang id=2]", "");
		s = s.Replace ("[lang id=3]", "");
		s = s.Replace ("[/lang]", "");

		s = s.Replace ("[c steelblue]", "[4682B4]");
		s = s.Replace ("[c blueviolet]", "[8A2BE2]");
		s = s.Replace ("[c tomato]", "[FF6347]");
		s = s.Replace ("[c hotpink]", "[FF69B4]");
		s = s.Replace ("[c blue]", "[00009E]");
		s = s.Replace ("[c brown]", "[A52A2A]");
		s = s.Replace ("[c cadetblue]", "[5F9EA0]");
		s = s.Replace ("[c mediumpurple]", "[9370DB]");
		 

		s = s.Replace ("[com]", "");
		s = s.Replace ("[/com]", "");


		s = s.Replace ("[/c]", "[-]");
		s = s.Replace ("[p]", "[i][00FF99]");
		s = s.Replace ("[/p]", "[-][/i]");
		return s;
	}
}