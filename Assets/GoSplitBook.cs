﻿using UnityEngine;
using System.Collections;
using System.IO;

public class GoSplitBook : MonoBehaviour {
	float t=0;
	StringReader file = null;
	public TextAsset f;
	public string s, copiedFile, copiedOnlyName, wordspath;
	public GameObject songs;
	string line;

	void Start () {
		t = 0;
		s = Application.dataPath+"/Resources/"+f.name+".txt";
		copiedOnlyName = Application.persistentDataPath+"/tyv-rus/"+f.name+"2015";
		copiedFile = Application.persistentDataPath+"/tyv-rus/"+f.name+"2015.txt";
		Directory.CreateDirectory(Path.GetDirectoryName(copiedFile));
		if (File.Exists (copiedFile)) File.Delete (copiedFile);
		File.Copy(s, copiedFile);

		wordspath = Application.persistentDataPath+"/tyv-rus/tyv-rus-words.txt";

		//createWords ();
		//songs.GetComponent<songs> ().Starter ();
	}


	void OnDrag () {
		t += Time.deltaTime;
		Debug.Log (t);
	}

	void OnDragOut () {
		if (t > 1) {
			if (transform.GetChild (0).GetComponent<UIWidget> ().alpha < 1f) transform.GetChild (0).GetComponent<UIWidget> ().alpha = 1;
			else transform.GetChild (0).GetComponent<UIWidget> ().alpha = 0;
		}
		t = 0;
	}

	void OnClick () {
			if (transform.GetChild (0).GetComponent<UIWidget> ().alpha == 1f) {		
			createWords();
			}
		}

	void createWords() {
		Debug.Log("New File!");
		StreamWriter word;
		file = new StringReader (f.text);
		Debug.Log(file+"ali");
		if (File.Exists (wordspath)) File.Delete (wordspath);
		word = File.CreateText (wordspath);
		
		if (file != null) {
			word.WriteLine ("#Тыва-орус сөстүк. Күжүгет Али ону кылган");
			word.WriteLine ("#Тувинско-русский словарь. Сделал Кужугет Али");
			word.WriteLine ("");
			while ((line = file.ReadLine())!=null) {
				if (!line.StartsWith ("	"))	if (line.Length != 0 && !line.StartsWith ("#")){
					word.WriteLine (line);
				}
			}
		}
		word.Close ();
	}
}