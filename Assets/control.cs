﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class control : MonoBehaviour {
	//public GameObject TPanel;
	public GameObject Ptext;
	public GameObject songs;

	public GameObject Panel;


	public int n;
	
	public string act;
	public delegate void Del (int x);

	public void help () {
		if (act == "help") {
			Ptext.SetActive(true);
			act = "unhelp";
		} else {
			Ptext.SetActive(false);
			act = "help";
		}
	}

	public void music () {
		if (act == "music") {
			Panel.SetActive (false);
			act = "nonmusic";
		} else {
            Panel.SetActive (true);
            act = "music";
        }
    }

	void Start () {
		n = songs.GetComponent<songs> ().n;
	}
}
