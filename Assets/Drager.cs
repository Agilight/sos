﻿using UnityEngine;
using System.Collections;

public class Drager : MonoBehaviour {
	public GameObject bkg;
	public GameObject tPanel;
	private Vector3 x;

	private float y, z, K;
	// Use this for initialization
	void OnPress () {
		K = 1.0f * (bkg.GetComponent<UIWidget> ().width-4) / Screen.width;
		Debug.Log(K);
		x = Input.mousePosition;
		y = gameObject.GetComponent<UIWidget> ().bottomAnchor.absolute;
		z = gameObject.GetComponent<UIWidget> ().topAnchor.absolute;
	}
	
	// Update is called once per frame
	void OnDrag () {
		bool b = gameObject.transform.localPosition.y - gameObject.GetComponent<UIWidget> ().height / 2 <= tPanel.transform.localPosition.y - tPanel.GetComponent<UIWidget> ().height / 2;
		bool c = gameObject.transform.localPosition.y + gameObject.GetComponent<UIWidget> ().height / 2 >= tPanel.transform.localPosition.y + tPanel.GetComponent<UIWidget> ().height / 2;
		if (b) {
			if (c) {
			gameObject.GetComponent<UIWidget> ().bottomAnchor.absolute = (int)(y + (Input.mousePosition.y - x.y) * K);
			gameObject.GetComponent<UIWidget> ().topAnchor.absolute = (int)(z + (Input.mousePosition.y - x.y) * K);
			} else {
				gameObject.GetComponent<UIWidget> ().bottomAnchor.absolute = (int)(y + 10 * K);
				gameObject.GetComponent<UIWidget> ().topAnchor.absolute = (int)(z + 10 * K);
			}
		}else {
			gameObject.GetComponent<UIWidget> ().bottomAnchor.absolute = (int)(y - 10 * K);
			gameObject.GetComponent<UIWidget> ().topAnchor.absolute = (int)(z - 10 * K);
		}



	}
}
