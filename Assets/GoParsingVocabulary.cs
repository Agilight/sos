﻿using UnityEngine;
using System.Collections;
using System.IO;

public class GoParsingVocabulary : MonoBehaviour {
	StringReader file = null;
	public bool gowords;
	public TextAsset f;
	public string wordspath, parseddata;
	string line;
	
	void Start () {

		parseddata = Application.dataPath+"/tyv-rus/parsed-data-2015-new.txt";
		wordspath = Application.dataPath+"/tyv-rus/tyv-rus-words-2015-new.txt";
		if (!File.Exists (Path.GetDirectoryName (wordspath))) {
			Directory.CreateDirectory (Path.GetDirectoryName (wordspath));
			Debug.Log("aaa");
		}
	}

	void OnClick () {
		if (gowords)
			createWords ();
		else
			ParsingData ();
	}

	void ParsingData() {
		Debug.Log("New File!");
		StreamWriter twrite;
		file = new StringReader (f.text);
		if (File.Exists (parseddata)) File.Delete (parseddata);

		twrite = File.CreateText (parseddata);
		if (file != null) {
			twrite.WriteLine ("#Тыва-орус сөстүк. Күжүгет Али ону кылган");
			twrite.WriteLine ("#Тувинско-русский словарь. Сделал Кужугет Али");
			twrite.WriteLine ("");
			while ((line = file.ReadLine())!=null) {
				if (!line.StartsWith ("#")){
					twrite.WriteLine (Parsing(line));
				}
			}
		}
		twrite.Close ();
	}

	void createWords() {
		Debug.Log("New File!");
		StreamWriter word;
		file = new StringReader (f.text);
		if (File.Exists (wordspath)) File.Delete (wordspath);
		word = File.CreateText (wordspath);

		if (file != null) {
			word.WriteLine ("#Тыва-орус сөстүк. Күжүгет Али ону кылган");
			word.WriteLine ("#Тувинско-русский словарь. Сделал Кужугет Али");
			word.WriteLine ("");
			while ((line = file.ReadLine())!=null) {
				if (!line.StartsWith ("	"))	if (line.Length != 0 && !line.StartsWith ("#")){
					word.WriteLine (line);
				}
			}
		}
		word.Close ();
	}


	string Parsing (string s) {
		s = s.Replace ("[m1]", "");
		s = s.Replace ("[m2]", " ");
		s = s.Replace ("[/m]", "");
		s = s.Replace ("[trn]", "");
		s = s.Replace ("[/trn]", "");
		s = s.Replace ("[!trs]", "");
		s = s.Replace ("[/!trs]", "");

		s = s.Replace ("[ex]", "[808080]");
		s = s.Replace ("[/ex]", "[-]");
		s = s.Replace ("[ref]", "[i][0099BB]");
		s = s.Replace ("[/ref]", "[-][/i]");
		
		s = s.Replace ("[lang id=1]", "");
		s = s.Replace ("[lang id=2]", "");
		s = s.Replace ("[lang id=3]", "");
		s = s.Replace ("[/lang]", "");
		
		s = s.Replace ("[c steelblue]", "[4682B4]");
		s = s.Replace ("[c blueviolet]", "[8A2BE2]");
		s = s.Replace ("[c tomato]", "[FF6347]");
		s = s.Replace ("[c hotpink]", "[FF69B4]");
		s = s.Replace ("[c blue]", "[00009E]");
		s = s.Replace ("[c brown]", "[A52A2A]");
		s = s.Replace ("[c cadetblue]", "[5F9EA0]");
		s = s.Replace ("[c mediumpurple]", "[9370DB]");
		
		
		s = s.Replace ("[com]", "");
		s = s.Replace ("[/com]", "");
		
		
		s = s.Replace ("[/c]", "[-]");
		s = s.Replace ("[p]", "[i][00FF99]");
		s = s.Replace ("[/p]", "[-][/i]");
		return s;
	}
}
